console.log("Hello World")

let firstName = "John";
console.log("First Name: " + firstName)

let lastName = "Smith";
console.log("Last Name: " + lastName)

let age = 30;
console.log("Age: " + age)


let hobbies = ["biking", "Mountain Climbing", "Swimming"];
console.log(hobbies);


let workAddress = {
	city: "Lincoln",
	houseNumber: 32,
	state: "Nebraska",
	street: "Washington"
}
console.log(workAddress);


console.log("John Smith is 30 years of age.")
function printDetails(firstName, lastName, age){
	console.log(firstName + lastName + "is " + age + "years of age.");
}
function argumentFunction(){
	console.log("This was printed inside of the function");
}
function invokeFunction(argumentFunction){
	argumentFunction();
}
invokeFunction(argumentFunction);
console.log(hobbies);
console.log(workAddress);

let isMarried = true;
console.log("The value of isMarried is: " + isMarried);